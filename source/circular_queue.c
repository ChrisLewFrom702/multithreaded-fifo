/*
*   @author Christopher Lewis
*   @file  
*   @date   5/23/2021
*
*   @brief 
*           Implementation file for the circular queue & its functions.
*               Please refer to the circular queue's respective .h file 
*               for documentation on its functions.
*
*/

#include <stdio.h>
#include <stdlib.h>
#include "../include/circular_queue.h"


//====================================================================
void initQueue(circular_queue_t *Queue, unsigned int queueSize)
{
    Queue->data = (__uint32_t*)malloc(sizeof(__uint32_t) * queueSize);

    if(Queue->data == NULL)
    {
        fprintf(stderr,"Error: Out of memory in file %s in function %s\n", __FILE__, __func__);
    }
    else
    {
        for (unsigned int i = 0 ; i < queueSize; i++)
        {
            Queue->data[i] = 0;
        }

        Queue->head = 0;
        Queue->tail = 0;
        Queue->maxLength = queueSize;
        
    }

}
//====================================================================
void push(circular_queue_t *Queue, __uint32_t payload)
{
    Queue->data[Queue->tail] = payload;

    Queue->tail = (Queue->tail + 1) % Queue->maxLength;
}
//====================================================================
__uint32_t pop(circular_queue_t *Queue)
{
    __uint32_t payload = 0;

    payload = Queue->data[Queue->head];
    Queue->head = (Queue->head + 1) % Queue->maxLength;

    return payload;

}
//====================================================================
void freeQueue(circular_queue_t *Queue)
{
    for(unsigned int i = 0 ; i < Queue->maxLength; i++)
    {
        Queue->data[i] = 0;
    }

    free(Queue->data);
}
//====================================================================
//
//  End of circular queue implementation file.
//
//====================================================================
