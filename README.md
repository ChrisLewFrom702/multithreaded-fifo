# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is the repo for a multithreaded, multi producer consumer FIFO.
 
	My FIFO includes the following features.

		- Allows command line configuration of number of consumers,producers size of FIFO container as well as number of payloads to be processed.
		- Is implemented with a circular queue under the hood 
		- Allows for READS & WRITES to occur at the same time.
		- Does not over write data, FIFO will block when attempting to push a payload when full.
	

### Why double mutex method? ###

	Most multithreaded implementations a FIFO queue normally put a lock around the entire data structure itself allowing  
	either a push/write or pop/read to occur one at a time, by treating the whole queue as a race condition.
	
	My implementation allows for both the readers & writers to operate on their respective parts of the queue at the same time.
	That is to say that instead of making the entire structure a race condition, I made both ends (head and tail) a race condition.
	
	This method allows a much greater throughput when given a larger FIFO/Queue size.
	
	
### How I Tested my FIFO ###
	
	I tested this FIFO's limitations by first testing the upper bounds of producers,consumers,payloads & queuesize.
	Simply to see if the upper bounds would break it.
	
	Next I used a few different methods to determine if ordering was correct & to check that no data was lost or repeated.
	
	For a full explanation of how I determined those things please see the bash script in this repo labeled compareFIFO.sh
	
	From a CPU usage standpoint I ran this my program infinitely and used the 'top' command on linux to see how much it was
	bottlenecking my CPU, topping out at about 7% to 8%. Which to me says I am putting my threads to sleep and waking them up
	in a CPU effecient manner.

### Build Instructions & Dependancies ###

* After cloning the repo run the following commands to correctly build the program without tests.

Note, you need latest version of CMAKE & GCC, make sure those are installed first.

		- cd /multithreaded-fifo
		- mkdir build
		- cmake ..
		- make -j8
		- ./MultithreadedFIFO_EXE -f [1-1000] -p [1-100] -c [1-100] -t [1-10,000]
			
* If you wish to run with tests follow these commands after cloning the repo.

		- cd /multithreaded-fifo
		- mkdir build
		- cmake -DFIFO_TEST=ON ..
		- make -j8
		- ./MultithreadedFIFO_EXE -f [1-1000] -p [1-100] -c [1-100] -t [1-10,000]

*	When you want to re-build simply run the following command from the build directory you've made.

		- rm -rf ../build/*

### Testing Instructions ###

	If you wish to test the program, after building and running the program with the '-DFIFO_TEST=ON' flag.
	Simply run the bash script in this repo called 'compareFIFO.sh'



