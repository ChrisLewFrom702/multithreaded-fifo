/*
*   @author Christopher Lewis
*   @file  
*   @date   5/19/2021
*
*   @brief 
*           This will be the main driver of the multithreaded-fifo.
*
*   Usage:
*          ./MultithreadedFIFO_EXE -f [1-1000] -p [1-100] -c [1-100] -t [1-100,000]
*           -f [1-1000]: Maximum size of FIFO container.
*           -p [1-100]: Number of producer threads.
*           -c [1-100]: Number of consumer threads.
*           -t [1-100,000]: Number of total payloads to be processed.
*
*   @note If you wish to comprehensively test this program refer to the README.md  or CMakeList.txt for build instructions/information.   
*      
*/

#include <stdio.h>
#include <pthread.h>
#include "../include/multithreaded-fifo.h"
#include "../include/circular_queue.h"


int main(int argc, char *argv[])
{

#ifdef FIFO_TEST


        consumerFileptr = fopen("consumerOut.txt", "w");
        producerFileptr = fopen("producerOut.txt", "w");

        if(consumerFileptr == NULL || producerFileptr == NULL)
        {
            fprintf(stderr,"Error, not enough memory to create output files.\n");
            return -1;
        }

#endif 


    if(checkArgs(argc,argv) == true)
    {
        // Initialize our data sturctures & variables.

        initQueue(&Queue, queueSize);


        sem_init(&semIsEmpty, 0 , queueSize);
        sem_init(&semIsFull, 0 , 0);

        payloadPOPCount = 0;
        payloadPUSHCount = 0;

        pthread_mutex_init(&queuePushMtx, NULL);
        pthread_mutex_init(&queuePopMtx, NULL);

        pthread_t producers[producerThreadCount];  
        pthread_t consumers[consumerThreadCount];     

        srand(time(NULL));  


        // Spawn our producers & consumers.
        for(unsigned int i = 0; i < producerThreadCount; i++)
        {
            pthread_create(&producers[i],NULL,&producerFunc,(void*)(long)(i + 1));
        }
        
        for(unsigned int i = 0; i < consumerThreadCount; i++)
        {
            pthread_create(&consumers[i],NULL,&consumerFunc, (void*)(long)(i + 1));
        }
        

        // Kill our producers & consumers.
        for(unsigned int i = 0; i < producerThreadCount; i++)
        {
            pthread_join(producers[i], NULL);
        }

        for(unsigned int i = 0 ; i < consumerThreadCount; i++)
        {
            pthread_join(consumers[i],NULL);
        }

        // Destory & de-allocate our variables, mutexes & semaphores.

        pthread_mutex_destroy(&queuePopMtx);
        pthread_mutex_destroy(&queuePopMtx);

        sem_destroy(&semIsFull);
        sem_destroy(&semIsEmpty);
        
        freeQueue(&Queue);
    }
    else
    {
        return -1; //< Incorrect arguments.
    }
    
#ifdef FIFO_TEST

    fclose(consumerFileptr);
    fclose(producerFileptr);        

#endif 
 

    return 0;

}