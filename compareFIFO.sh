#!/bin/bash

#
#   @author - Christopher Lewis
#   @date 5/23/2021
#
#   Simple script for testing if my multithreaded fifo solution worked.
#       Below are the 3 main checks that this script is doing to determine
#       if all payloads were produced & consumed correctly.
#      
#   Check 1: Line count check
#           - Script will run through consumer & producer output files and count 
#               how many lines are in each file. The assumption being if
#               their line counts are the same, no payloads were lost.
#
#   Check 2: Payload sum check
#           - Script will take all the payloads reported by the producers & consumers
#               add them up and compare their sums. The assumption being if
#               their sums are equal, there were no repeated or lost payloads.
#   
#   Check 3: FIFO order check
#           - Script will compare both output files one line at a time to eachother
#               if FIFO order was maintained, each payload from any given line from one
#               of the output files, should the match the payload from the same line on the 
#               other output file. (If a producer pushed 42 on line 7, a consumer should receive 42 on line 7)
#


#
#   Find our consumerOut.txt file.
#
find ~ -name "consumerOut.txt" > /dev/null

if [  $? -ne 0 ]; then

    echo "Could not find 'consumerOut.txt"
    echo "Please re-build or re-run the program and try again."
    echo "Make sure -DTEST_FIFO=ON flag was set when building."
    exit 1

fi

#
#   Find our producerOut.txt file.
#
find ~ -name "producerOut.txt" > /dev/null

if [  $? -ne 0 ]; then

    echo "Could not find 'consumerOut.txt"
    echo "Please re-build or re-run the program and try again."
    echo "Make sure -DTEST_FIFO=ON flag was set when building."
    exit 1

fi

#
#   Start of Check 1: Line count check.
#

CONSUMER_OUT_FILE=($(find ~ -name "consumerOut.txt"))
PRODUCER_OUT_FILE=($(find ~ -name "producerOut.txt"))

echo "Found both output files, will now compare line counts"

consumerLineCount=$(wc -l < ${CONSUMER_OUT_FILE})
producerLineCount=$(wc -l < ${PRODUCER_OUT_FILE})

if  [ ${consumerLineCount} -eq ${producerLineCount} ]; then
    echo "Both files have the same line count!"
    echo "Producer file line count is ${producerLineCount}"
    echo "Consumer file line count is ${consumerLineCount}"
else
    echo "Error, the output files do not have the same line count."
    echo "Producer file line count is ${producerLineCount}"
    echo "Consumer file line count is ${consumerLineCount}"
    exit 1
fi

echo "Now will add up payloads pushed & recieved in each file to compare their sums."
echo "If their sums and line counts match, we can safely assume no data was lost or repeated."

#
#   Start of Check 2: Payload sum check
#   
#
declare -i producer_sum=0
declare -i consumer_sum=0
declare -i currNum=0

while read line; do

    currNum="$(grep -o '[[:digit:]]*' <<< ${line})"
    consumer_sum=consumer_sum+currNum;

done < ${CONSUMER_OUT_FILE}

declare -i currNum=0

while read line; do

    currNum="$(grep -o '[[:digit:]]*' <<< ${line})"
    producer_sum=producer_sum+currNum;

done < ${PRODUCER_OUT_FILE}

if [ ${producer_sum} -eq ${consumer_sum} ]; then

    echo "The consumers sum and producers sum DID matched!"
    echo "The consumers sum was ${consumer_sum}"
    echo "The producer sum was ${producer_sum}"
else
    echo "The consumers sum and producers sum DID NOT match!"
    echo "The consumers sum was ${consumer_sum}"
    echo "The producer sum was ${producer_sum}" 
    exit 1
fi

#
#   Check 3: FIFO order check
#
#

echo "Now will do final FIFO order check."
echo "Will check each payload pushed & popped was accounted for line by line."

declare -i producedPayload=0
declare -i consumedPayload=0

while read producerLine <&3 && read consumerLine <&4; do

    producedPayload="$(grep -o '[[:digit:]]*' <<< ${producerLine})"
    consumedPayload="$(grep -o '[[:digit:]]*' <<< ${consumerLine})"
    
    if [ ${consumedPayload} -ne ${producedPayload} ]; then
        
        echo "Error,a payload produced did not match a payload consumed."
        echo "The payloads were not produced/consumed in FIFO order."
        exit 1

    fi

done 3<${PRODUCER_OUT_FILE} 4<${CONSUMER_OUT_FILE}


#
#   End of all checks, if we make it here we are good.
#

echo "All checks done"
echo "File line counts & sums match, assuring no data loss or repeated"
echo "Each payload was produced and consumed in correct FIFO order."

exit 0