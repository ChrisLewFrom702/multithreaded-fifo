/*
*   @author Christopher Lewis
*   @file  
*   @date   5/23/2021
*
*   @brief 
*           The header file for the circular queue structure, including explanations for 
*               the queue's functions and explaining how it should and should not be used.
*      
*/

#ifndef CIRCULAR_QUEUE_H
#define CIRCULAR_QUEUE_H


/*
*   @brief Circular queue data structure.
*
*   @note Inspired by the following implementation of a 
*           circular_queue using an internal buffer.
*
*           - https://www.geeksforgeeks.org/circular-queue-set-1-introduction-array-implementation
*
*/
    typedef struct circular_queue
    {
        __uint32_t *data;       //< Internal array our queue will hold its data, which will be allocated on the heap.
        unsigned int head;      //< The index at which the next POP/READ will operate on.
        unsigned int tail;      //< The index at which the next PUSH/WRITE will operate on.
        unsigned int maxLength; //< Maximum length for the queue.

    } circular_queue_t;

circular_queue_t Queue; //< Global queue to be passed around to its respective functions.

/*
*   @brief Function will initialize the global circular queue.
*           Allocating the queue's internal array to user specified size
*               and zero-ing out each element.
*           Will also assign 'head' & 'tail' indexes to 0 and
*               'maxLength' to passed in 'queueSize' 
*
*   @param[in] Queue - of type pointer to circular_queue_t
*   @param[in] queueSize - of type unsigned int used to determine maximum size of queue & how much space is allocated for its internal array.
*
*   @param[out] NONE - Function is void, no return value.
*   
*/
    void initQueue(circular_queue_t *Queue, unsigned int queueSize);
/*
*   @brief Function will PUSH a given payload on to the circular queue at 
*           the index 'tail'.
*
*          Function uses modular math to keep the 'tail' index wrapped around the queue's max length.
*         
*          This function is functionally equivalent to the basic 'Enqueue' function
*           used in most common queue documentation.
*
*   @note - This function should not ever be used without proper syncronization primitives 
*               surrounding it. Using this function without proper syncronization may cause
*               incorrect data & over-writing of data.
*
*   @param[in]  Queue - of type pointer to circular_queue_t
*   @param[in]  payload - of type __int32_t, the actual data being put onto the queue.
*
*   @param[out] NONE - Function is void, no return value.
* 
*/
    void push(circular_queue_t *Queue, __uint32_t payload);
/*
*   @brief Function will POP a given payload on to the circular queue at 
*           the index 'head'.
*
*          Function uses modular math to keep the 'head' index wrapped around the queue's max length.
*         
*          This function is functionally equivalent to the basic 'Dequeue' function
*           used in most common queue documentation.
*
*   @note - This function should not ever be used without proper syncronization primitives 
*               surrounding it. Using this function without proper syncronization may cause
*               incorrect data & over-writing of data.
*
*   @param[in]  Queue - of type pointer to circular_queue_t
*
*   @param[out]  payload - of type __int32_t, the actual data being removed from the queue.
*
*/
    __uint32_t pop(circular_queue_t *Queue);
/*
*   @brief Function will cleanly de-allocate the passed in queue,
*           zero-ing out the internal array & freeing the allocated memory.
*   
*   @param[in]  Queue - of type pointer to circular_queue_t
*
*   @param[out] NONE - Function is void, no return value.
*
*/
    void freeQueue(circular_queue_t *Queue);


#endif //< CIRCULAR_QUEUE_H