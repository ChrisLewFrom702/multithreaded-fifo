/*
*   @author Christopher Lewis
*   @file  
*   @date   5/19/2021
*
*   @brief 
*
*       This is the header file for my multithreaded FIFO solution, here I put all my related functions and structures
*       along with documentation on any tricky calculations that may be done within each function.
*       
*
*/


#ifndef MULTITHREADED_FIFO_H
#define MULTITHREADED_FIFO_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <semaphore.h>
#include <pthread.h>

#include "../include/circular_queue.h"

#define MAX_PAYLOAD_COUNT    100000  
#define MAX_PRODUCER_THREADS 100
#define MAX_CONSUMER_THREADS 100
#define MAX_QUEUE_SIZE       1000


pthread_mutex_t queuePushMtx;   //< Mutex to syncronize pushing onto the Queue.
pthread_mutex_t queuePopMtx;    //< Mutex to syncronize popping from the Queue.

sem_t semIsFull;                //< Semaphore to communicate that the Queue has no data ready to be consumed.
sem_t semIsEmpty;               //< Semaphore to communicate that the Queue has some data to be processed.

FILE  *consumerFileptr;         //< File pointer for consumers to print consumed payloads to.
FILE  *producerFileptr;         //< File pointer for producers to print produced payloads to.

unsigned int consumerThreadCount;   //< Number of consumer threads to be created.
unsigned int producerThreadCount;   //< Number of producer threads to be created.
unsigned int payloadCount;          //< Amount of payloads processed/transported by Queue.

unsigned int payloadPOPCount;       //< Number of payloads POPPED so far, used to know when to stop program.
unsigned int payloadPUSHCount;      //< Number of payloads PUSHED so far, used to know when to stop program.

unsigned int queueSize;             //< Maximum size of queue, used for initializing queue at runtime.



/*
*   @brief Producer function, used to create a payload & pushed it onto the 
*           global Queue.
*           Below is the general flow for the producer thread(s).
*   
*   1) All threads will reach 'semIsEmpty' semaphore and only pass it when there is
*       room on the queue to put a payload. 
*
.   2) Threads that pass step 1 will race for the 'queuePushMtx' and which ever thread grabs it
*       will move on to step 3 while the other threads sleep/wait.
*
*   3) The thread that passes step 3 will do the follow.
*
*                   - create a payload 
*                   - push it onto the queue
*                   - increment number of payloads pushed.
*                   - unlock its mutex
*                   - 'post' that there is a payload ready to be processed.
*
*   - if there are more payloads to be processed the process repeats, else the thread exits.
*
*   @param[in] threadid - of type void pointer, can be cast to integer to identify current threadID.
*
*   @param[out] NONE - function void, no return value.       
*/
    void *producerFunc(void *threadId);

/*
*   @brief Consumer function, will grab payload from global Queue in FIFO order.

*           Below is the general flow for the consumer thread(s).
*   
*   1) All threads will reach 'semIsFull' semaphore and only pass it when there is
*       some data on the queue to be processed.
*
.   2) Threads that pass step 1 will race for the 'queuePopMtx' and which ever thread grabs it
*       will move on to step 3 while the other threads sleep/wait.
*
*   3) The thread that passes step 3 will do the follow.
*
*                   - pop payload from Queue.                   
*                   - increment number of payloads POPPED.
*                   - unlock its mutex
*                   - 'post' that there is a payload was removed from queue.
*
*   - if there are more payloads to be processed the process repeats, else the thread exits.
*
*   @param[in] threadid - of type void pointer, can be cast to integer to identify current threadID.
*
*   @param[out] NONE - function void, no return value.       
*/
    void *consumerFunc(void *threadId);

/*
*   @brief Function will check command line arguments for both
*           correct argument count and to ensure passed in values 
*           for thread counts, queuesize & payload count fit within
*           defined bounds.
*
*   @param[in] argc, of type int, number of arguments on command line.
*   @param[in] argv, of type pointer to character pointers, vector of arguments on command line.
*
*   @param[out] boolean, true if all arugments passed bounds checking, false if not.
*
*/
    bool checkArgs(int argc,char *argv[]);

/*
*   @brief Simple usage message explaining how the program is meant to be
*       executed, including relevant flags, their meanings & upper/lower bounds.
*
*   @param[in] NONE - function takes no arguments.
*
*   @param[out] NONE - function void, no return value.       
*/
    void printUsage();


#endif //< MULITTHREADED_FIFO_H