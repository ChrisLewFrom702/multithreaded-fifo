/*
    @author Christopher Lewis
*   @file  
*   @date   5/19/2021
*
*   @brief 
*
*       This is the implementation file for my multithreaded FIFO solution. Please refer to the header file for 
*            information about any tricky calculations within the following functions.
*
*/


#include "../include/multithreaded-fifo.h"

//====================================================================
void *producerFunc(void *threadId)
{
    while(true)
    {
        sem_wait(&semIsEmpty);  //< Threads will only pass when Queue has room to push payloads onto it.
        pthread_mutex_lock(&queuePushMtx);

        if(payloadPUSHCount < payloadCount)
        {
            __uint32_t payload = (rand() % 100) + 1;  //< Just to ensure we don't push 0.
            push(&Queue, payload);
            fprintf(producerFileptr, "PRODUCER pushed payload - %u\n",payload);
            
            payloadPUSHCount++;
            pthread_mutex_unlock(&queuePushMtx);
            sem_post(&semIsFull); 
        }
        else
        {
            sem_post(&semIsFull);               //< These final 'posts' are to ensure all threads exit & to avoid deadlocks.
            sem_post(&semIsEmpty);
            pthread_mutex_unlock(&queuePushMtx);
            break;
        }
        
    }
    
}
//====================================================================
void *consumerFunc(void *threadId)
{        
        while(true)
        {
            sem_wait(&semIsFull); //< will 'wait' if queue has no data to consume.
            pthread_mutex_lock(&queuePopMtx);

            if(payloadPOPCount < payloadCount)
            {
                __uint32_t payload = 0;
                payload = pop(&Queue);
                fprintf(consumerFileptr,"\tCONSUMER received payload - %u\n",payload);
                payloadPOPCount++;
                pthread_mutex_unlock(&queuePopMtx);
                sem_post(&semIsEmpty); //< will 'post' that data was removed from queue.
            }
            else
            {
                sem_post(&semIsFull);           //< Post to waiting consumers that all payloads are processed.
                pthread_mutex_unlock(&queuePopMtx);
                break;
            }
            
        }
        
}
//====================================================================
bool checkArgs(int argc, char *argv[])
{

    if(argc != 9)
    {
        fprintf(stderr,"Error, wrong number of arguments!\n");
        printUsage();
        return false;
    }
    
#ifndef FIFO_TEST
    consumerFileptr = stdout;
    producerFileptr = stdout;
#endif

    queueSize           = atoi(argv[2]);
    producerThreadCount = atoi(argv[4]);
    consumerThreadCount = atoi(argv[6]);
    payloadCount        = atoi(argv[8]);
    
    if(queueSize <= 0 || queueSize > MAX_QUEUE_SIZE)
    {
        fprintf(stderr,"Error, invalid FIFO size.\n");
        printUsage();
        return false;
    }

    if(producerThreadCount <= 0 || producerThreadCount > MAX_PRODUCER_THREADS)
    {
        fprintf(stderr,"Error, invalid PRODUCER thread count");
        printUsage();
        return false;
    }

    if(consumerThreadCount <= 0 || consumerThreadCount > MAX_CONSUMER_THREADS)
    {
        fprintf(stderr,"Error, invalid CONSUMER thread count.\n");
        printUsage();
        return false;
    }

    if(payloadCount <= 0 || payloadCount > MAX_PAYLOAD_COUNT)
    {
        fprintf(stderr,"Error, invalid payloads wanted to be processed.\n");
        printUsage();
        return false;
    }

    return true;
}
//====================================================================
void printUsage()
{
    fprintf(stderr,"Usage: ./MultithreadedFIFO_EXE -f [1-1000] -p [1-100] -c [1-100] -t [1-100,000]\n");
    fprintf(stderr," -f [1-1000]: Maximum size of FIFO container.\n");
    fprintf(stderr," -p [1-100]: Number of producer threads.\n");
    fprintf(stderr," -c [1-100]: Number of consumer threads.\n");
    fprintf(stderr," -t [1-100,000]: Number of total payloads to be processed.\n");
}
//====================================================================
//
//  End of multithreaded-fifo implementation file.
//
//====================================================================